import Foundation
import CoreData


extension Note {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Note> {
        return NSFetchRequest<Note>(entityName: "Note")
    }

    @NSManaged public var noteData: Data
    @NSManaged public var noteEditDate: Date
    @NSManaged public var noteName: String
    @NSManaged public var actualSize: Int

}

extension Note : Identifiable {

}
