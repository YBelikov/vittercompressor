import Foundation
import CoreData

class CoreDataStack {

    private init() {}

    static let shared: CoreDataStack = .init()
    // MARK: - Core Data stack

    lazy var viewContext: NSManagedObjectContext = {
        return container.viewContext
    }()

    lazy var container: NSPersistentContainer = {

        let container = NSPersistentContainer(name: "VitterCompressor")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {

                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func save() {
        let context = container.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func deleteAll(entityName: String) {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        let context = container.newBackgroundContext()
        do {
            try context.execute(deleteRequest)
        } catch let error as NSError {
            print(error)
        }
    }
}
