import UIKit

struct UserInfo: Decodable {
    
    lazy var image: UIImage? = {
        return UIImage(named: self.bundleImageName)
    }()
    
    let name: String
    let group: String
    let recordBook: String
    let about: String
    
    private let bundleImageName: String
    
}
