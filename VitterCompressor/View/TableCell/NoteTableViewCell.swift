import UIKit

class NoteTableViewCell: UITableViewCell {

    @IBOutlet
    private weak var noteName: UILabel!
    @IBOutlet
    private weak var noteEditDate: UILabel!
    @IBOutlet
    private weak var noteSize: UILabel!
    @IBOutlet
    private weak var noteActualSize: UILabel!
    
    private var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        return formatter
    }()

    func install(_ note: Note) {
        self.noteName.text = note.noteName
        self.noteEditDate.text = formatter.string(from: note.noteEditDate)
        self.noteSize.text = "Compressed: " + "\(note.noteData.count)"
        self.noteActualSize.text = "Actual: " + "\(note.actualSize)"
    }

}
