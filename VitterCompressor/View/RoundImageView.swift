import UIKit

class RoundImageView: UIImageView {
    
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let cornerRadius = self.frame.width
        let bPath = UIBezierPath(roundedRect: self.bounds,
                                 byRoundingCorners: [.allCorners],
                                 cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let mask = CAShapeLayer()
        mask.path = bPath.cgPath
        self.layer.mask = mask
    }
    
}
