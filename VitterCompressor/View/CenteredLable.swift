
import UIKit

class CenteredLable: UILabel {
    
    init() {
        super.init(frame: .zero)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        self.numberOfLines = 0
        self.sizeToFit()
        self.textAlignment = .center
        self.translatesAutoresizingMaskIntoConstraints = false
    }
}
