class TreePrinter
{

    private var root: HuffmanTreeNode;

    private var indent: Int;
    public init()
    {
        root = HuffmanTreeNode()
        indent = 5;
    }
    
    public func printTree(root: HuffmanTreeNode) {

        self.root = root;

        preorder(currentNode: root, lastChild: true, previousIndentation: 0);
    }


    public func preorder(currentNode: HuffmanTreeNode?, lastChild: Bool, previousIndentation: Int)
    {

        if currentNode !== nil
        {

            if (currentNode === root) {
                print(String(format: "%" + String(self.indent) + "s", "") +  "└── " + printNode(node: currentNode!));
            }
            else if (lastChild) {
                print(String(format: "%" + String(self.indent) + "s", "") +  "└── " + printNode(node: currentNode!));

            }
            else {
                print(String(format: "%" + String(self.indent) + "s", "") +  "├── " + printNode(node: currentNode!));
            }
            self.indent += 8;
            preorder(currentNode: currentNode!.getLeftChild(), lastChild: false, previousIndentation: self.indent - 8);
            preorder(currentNode: currentNode!.getRightChild(), lastChild: true, previousIndentation: self.indent - 8);
            self.indent -= 8;
        }
    }

    public func printNode(node: HuffmanTreeNode) -> String
    {
        var part = "null"
        if node.getValue() != nil
        {
            part = node.getValue()!
        }
        return String(node.getFrequency()) + " |" + part + "| " + String(node.getNodeId());
    }
}
