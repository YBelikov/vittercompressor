import Foundation

class FrequencyTable
{
    private var table: [Character: Int]
    
    public init()
    {
        table = [Character: Int]()
    }
    
    public func calculate(text: String)
    {
        for char in text
        {
            if  table[char] != nil
            {
                table[char] = table[char]! + 1
            }
            else
            {
                table[char] = 1
            }
        }
    }
    
    public func frequencies() -> [Character: Int]
    {
        return table;
    }
    
}
