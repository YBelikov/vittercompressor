import Foundation

class HuffmanTree
{
    private var nextNodeId: Int
    private var root: HuffmanTreeNode
    private var NYTNode: HuffmanTreeNode
    private var nodes = [String: HuffmanTreeNode]()
    private var nodeWeightGroups = [Int: [HuffmanTreeNode]]()
    public init()
    {
        let NYT = HuffmanTreeNode(nodeId: 256, value: "")
        NYT.setFrequency(frequency: 0)
        self.root = NYT;
        self.NYTNode = NYT;
        self.nextNodeId = 255;
     }
    
    public func getHuffmanCode(valueToGet: String) -> String
    {
        
        var result = "";
        var rootNodeFound = false;
        var currentNode, parentNode: HuffmanTreeNode;
        if valueToGet == ""
        {
            currentNode = self.NYTNode;
            if self.isRoot(node: self.NYTNode)
            {
                return "0"
            }
        }
        else
        {
            currentNode = self.nodes[valueToGet]!
        }
        parentNode = currentNode.getParentNode()!
        
        while rootNodeFound == false
        {
            result = parentNode.getRightChild() === currentNode ? "1" + result: "0" + result
            if self.isRoot(node: parentNode)
            {
                rootNodeFound = true;
            }
            else
            {
                currentNode = parentNode
                parentNode = currentNode.getParentNode()!
            }
        }
        return result
    }
    
    public func addCharToTree(valueToAdd: String)
    {
        //print(self.nodeWeightGroups)
        var nodeToEdit = self.getNodeFromTree(value: valueToAdd)
        var nodeHolder: HuffmanTreeNode? = nil;
        var rootNodeFound = false
        
        if nodeToEdit == nil
        {
            nodeToEdit = self.createNewNode(value: valueToAdd)
            if self.isRoot(node: nodeToEdit!.getParentNode()!) == false
            {
                self.addNodeToList(node: (nodeToEdit!.getParentNode())!, newFrequency: 1)
            }
            self.addNodeToList(node: nodeToEdit!, newFrequency: 1)
            nodeToEdit = (nodeToEdit!.getParentNode())!
        }
        else
        {
            self.updateNode(node: nodeToEdit!, newFrequency: (nodeToEdit?.getFrequency())! + 1)
            nodeToEdit!.incrementFrequncy()
        }
        nodeHolder = nodeToEdit
        
        if self.isRoot(node: nodeToEdit!) == true
        {
            rootNodeFound = true;
        }
        while(!rootNodeFound)
        {
            nodeHolder = nodeHolder!.getParentNode()!;

            //root is not in any weight groups so all we need to do with the root node is update its frequency
            if self.isRoot(node: nodeHolder!) == true
            {
                rootNodeFound = true;
            }
            else
            {
                self.updateNode(node: nodeHolder!, newFrequency: nodeHolder!.getFrequency() + 1);
            }
            nodeHolder!.incrementFrequncy();

            if self.isRoot(node: nodeHolder!) == true    
            {
                rootNodeFound = true;
            }
        }
        
    }
    
    private func createNewNode(value: String) -> HuffmanTreeNode
    {
        var newNode = HuffmanTreeNode(nodeId: self.nextNodeId, value: value)
        var newParentNode = HuffmanTreeNode(nodeId: self.NYTNode.getNodeId(), leftChild: self.NYTNode, rightChild: newNode)
        self.nextNodeId -= 2
        self.NYTNode.setNodeId(nodeId: newParentNode.getNodeId() - 2)
        newNode.setParentNode(parentNode: newParentNode)
        
        if self.NYTNode.getParentNode() != nil
        {
            if self.NYTNode.getParentNode()!.getLeftChild() === self.NYTNode
            {
                self.NYTNode.getParentNode()!.setLeftChild(child: newParentNode)
                newParentNode.setParentNode(parentNode: self.NYTNode.getParentNode()!)
            }
            else if self.NYTNode.getParentNode()!.getRightChild() === self.NYTNode
            {
                newParentNode.setParentNode(parentNode: self.NYTNode.getParentNode()!)
                self.NYTNode.getParentNode()!.setRightChild(child: newParentNode)
            }
            
        }
        else
        {
            self.root = newParentNode
        }
        self.NYTNode.setParentNode(parentNode: newParentNode)
        self.nodes[value] = newNode
        return newNode
        
    }
    
    private func addNodeToList(node: HuffmanTreeNode, newFrequency: Int)
    {
        var placeFound = false;
        var stepper = 0
        var currentStepperNode: HuffmanTreeNode? = nil
        if  self.nodeWeightGroups[newFrequency]
                == nil
        {
            self.nodeWeightGroups[newFrequency] = [HuffmanTreeNode]()
            self.nodeWeightGroups[newFrequency]!.insert(node, at: 0)
           
        }
        else
        {
            if self.nodeWeightGroups[newFrequency]!.isEmpty == true
            {
                self.nodeWeightGroups[newFrequency]!.insert(node, at: 0)
            }
            else
            {
                while placeFound == false
                {
                    currentStepperNode = self.nodeWeightGroups[newFrequency]![stepper]
                    if (currentStepperNode?.getNodeId())! > node.getNodeId()
                    {
                        self.nodeWeightGroups[newFrequency]!.insert(node, at: stepper)
                        placeFound = true
                    }
                    stepper = stepper + 1
                    if self.nodeWeightGroups[newFrequency]!.count <= stepper
                    {
                        self.nodeWeightGroups[newFrequency]!.append(node)
                        placeFound = true
                    }
                }
            
            }
        }
    }
    
    private func updateNode(node: HuffmanTreeNode, newFrequency: Int)
    {
        var listToEdit = [HuffmanTreeNode]()
        if self.isRoot(node: node.getParentNode()!)
        {
            if self.isHighestWeightInGroup(nodeToCheck: node) == false
            {
                self.editListPosition(node: node, indexFromEnd: 0, newFrequency: newFrequency)
            }
            else
            {
                self.nodeWeightGroups[node.getFrequency()]?.removeLast()
                self.addNodeToList(node: node, newFrequency: node.getFrequency() + 1)
            }
        }
        else if self.isHighestWeightInGroup(nodeToCheck: node) == false && self.parentIsHighetsWeightInGroup(nodeToCheck: node) == false
        {
            self.editListPosition(node: node, indexFromEnd: 0, newFrequency: newFrequency)
        }
        else if self.parentIsHighetsWeightInGroup(nodeToCheck: node) == true && self.isSecondHighestInWeightGroup(nodeToCheck: node) == false
        {
            self.editListPosition(node: node, indexFromEnd: 1, newFrequency: newFrequency)
        }
        else if self.parentIsHighetsWeightInGroup(nodeToCheck: node) == true && self.isSecondHighestInWeightGroup(nodeToCheck: node) == true
        {
            self.nodeWeightGroups[node.getFrequency()]!.remove(at:  self.nodeWeightGroups[node.getFrequency()]!.count - 2)
            self.addNodeToList(node: node, newFrequency: newFrequency)
            //self.nodeWeightGroups[node.getFrequency()] = listToEdit
        }
        else
        {
            self.nodeWeightGroups[node.getFrequency()]?.removeLast()
            self.addNodeToList(node: node, newFrequency: newFrequency)
        }
    }
    
    private func editListPosition(node: HuffmanTreeNode, indexFromEnd: Int, newFrequency: Int)
    {
        var freq = node.getFrequency()
         var position = -1;
        if (self.nodeWeightGroups[freq]!.firstIndex(where: { (item) -> Bool in
            item === node
        })) != nil
        {
            position = (self.nodeWeightGroups[freq]!.firstIndex(where: { (item) -> Bool in
                item === node
            }))!
        }
        else
        {
            print("error")
        }

        var highestNode = self.nodeWeightGroups[freq]!.remove(at: self.nodeWeightGroups[freq]!.count - indexFromEnd - 1)
        self.nodeWeightGroups[freq]?[position] = highestNode
        //self.nodeWeightGroups[freq] = firstList
        self.addNodeToList(node: node, newFrequency: newFrequency)
        self.swapNodesInTree(firstNode: node, secondNode: highestNode)
    }
    
    private func swapNodesInTree(firstNode: HuffmanTreeNode, secondNode: HuffmanTreeNode)
    {
        self.swapIds(first: firstNode, second: secondNode)
        self.swapParents(first: firstNode, second: secondNode)
    }
    
    
    private func swapIds(first: HuffmanTreeNode, second: HuffmanTreeNode)
    {
        var idSwapSpace = first.getNodeId()
        first.setNodeId(nodeId: second.getNodeId())
        second.setNodeId(nodeId: idSwapSpace)
    }
    
    private func swapParents(first: HuffmanTreeNode, second: HuffmanTreeNode)
    {
        var firstParentNode = first.getParentNode()!;
        var secondParentNode = second.getParentNode()!;
        first.setParentNode(parentNode: secondParentNode);
        second.setParentNode(parentNode: firstParentNode);
        if(firstParentNode.getRightChild()! === first)
        {
            firstParentNode.setRightChild(child: second);
        }
        else if(firstParentNode.getLeftChild()! === first)
        {
            firstParentNode.setLeftChild(child: second);
        }
        if(secondParentNode.getRightChild()! === second)
        {
            secondParentNode.setRightChild(child: first);
        }
        else if(secondParentNode.getLeftChild()! === second)
        {
            secondParentNode.setLeftChild(child: first);
        }
    }
    
    private func parentIsHighetsWeightInGroup(nodeToCheck: HuffmanTreeNode) -> Bool
    {
       
        return nodeToCheck.getParentNode()! === self.nodeWeightGroups[nodeToCheck.getFrequency()]?.last
    }
    
    private func isSecondHighestInWeightGroup(nodeToCheck: HuffmanTreeNode) -> Bool
    {
        return  self.nodeWeightGroups[nodeToCheck.getFrequency()]![self.nodeWeightGroups[nodeToCheck.getFrequency()]!.count - 2] === nodeToCheck
    }
    
    private func isHighestWeightInGroup(nodeToCheck: HuffmanTreeNode) -> Bool
    {
       
        return self.nodeWeightGroups[nodeToCheck.getFrequency()]?.last === nodeToCheck
    }
    
    private func getNodeFromTree(value: String) -> HuffmanTreeNode?
    {
        return self.nodes[value]
    }
    
    public func stringExists(value: String) -> Bool
    {
        var val = nodes[value]
        return val !== nil
    }
    
    private func isRoot(node: HuffmanTreeNode) -> Bool
    {
        return node === self.root
    }
    
    public func getNextNode(prev: HuffmanTreeNode, index: Character) -> HuffmanTreeNode
    {
        var result = HuffmanTreeNode()
        var bp = ""
        if index == "0"
        {
            bp = ""
            result = prev.getLeftChild()!
        }
        else
        {
            bp = ""
            result = prev.getRightChild()!
        }
        bp = ""
        return result
    }
    
    public func rootIsNYT() -> Bool
    {
        return self.isRoot(node: self.NYTNode)
    }
    
    public func isLeaf(node: HuffmanTreeNode) -> Bool
    {
        return node.getLeftChild() === nil
    }
    
    public func isNYT(node: HuffmanTreeNode) -> Bool
    {
        return node === self.NYTNode
    }
    
    public func getRoot() -> HuffmanTreeNode
    {
        return self.root
    }
}

