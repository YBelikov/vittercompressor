import Foundation


class HuffmanTreeNode : NSCopying
{
    
    private var frequency: Int
    private var nodeId: Int;
    private var value: String?;
    private var leftChild: HuffmanTreeNode?;
    private var rightChild: HuffmanTreeNode?;
    private var parentNode: HuffmanTreeNode?;
    
    public init(nodeId: Int, leftChild: HuffmanTreeNode, rightChild: HuffmanTreeNode )
    {
        self.nodeId = nodeId;
        self.leftChild = leftChild;
        self.rightChild = rightChild;
        self.frequency = leftChild.getFrequency() + rightChild.getFrequency();
        self.value = nil;
        self.parentNode = nil;
    }
    
    public init(nodeId: Int, value: String)
    {
        self.nodeId = nodeId;
        self.value = value;
        self.frequency = 1;
        self.leftChild = nil;
        self.rightChild = nil;
        self.parentNode = nil;
    }
    
    public init()
    {
        self.nodeId = -1;
        self.leftChild = nil;
        self.rightChild = nil;
        self.frequency = -1;
        self.value = nil;
        self.parentNode = nil;
    }
    
    public func setParentNode(parentNode: HuffmanTreeNode)
    {
        self.parentNode = parentNode;
    }
    
    public func getParentNode() -> HuffmanTreeNode?
    {
        return self.parentNode;
    }
    
    public func setFrequency(frequency: Int)
    {
        self.frequency = frequency;
    }
    
    public func getFrequency() -> Int
    {
        return self.frequency;
    }
    
    public func incrementFrequncy()
    {
        self.frequency = self.frequency + 1;
    }
    
    public func setRightChild(child: HuffmanTreeNode)
    {
        self.rightChild = child
    }
    
    
    public func getRightChild() -> HuffmanTreeNode?
    {
        return self.rightChild;
    }
    
    public func setLeftChild(child: HuffmanTreeNode)
    {
        self.leftChild = child
    }
    
    public func getLeftChild() -> HuffmanTreeNode?
    {
        return self.leftChild;
    }
    
    public func setValue(value: String)
    {
        self.value = value;
    }
    
    public func getValue() -> String?
    {
        return self.value;
    }
    
    public func setNodeId(nodeId: Int)
    {
        self.nodeId = nodeId;
    }
    
    public func getNodeId() -> Int
    {
        return self.nodeId;
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = HuffmanTreeNode()
        copy.frequency = self.frequency
        copy.nodeId = self.nodeId
        copy.parentNode = self.parentNode;
        copy.leftChild = self.leftChild
        copy.rightChild = self.rightChild
        copy.value = self.value;
        return copy;
    }
    
   
}
