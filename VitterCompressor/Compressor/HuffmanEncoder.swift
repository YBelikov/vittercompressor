import Foundation

class HuffmanEncoder
{
    private var tree: HuffmanTree
    private var writer: BitWriter
    
    public init()
    {
        tree = HuffmanTree()
        writer = BitWriter()
    }
    
    public func encode(text: String) -> Data
    {
        var huffmanCode = ""
        var codeToAdd = ""
        for char in text
        {
            let bits = bitRepresentation(value: Int(char.asciiValue!))
            huffmanCode += getHuffmanCode(valueToBeAdded: bits)
            if huffmanCode.count >= 8
            {
                codeToAdd = String(huffmanCode.prefix(8))
                let index =  huffmanCode.index(huffmanCode.endIndex, offsetBy: -(huffmanCode.count - 8))
                huffmanCode = String(huffmanCode.suffix(from: index))
                writeCode(code: codeToAdd)
            }
        }
        if huffmanCode.count < 8
        {
            huffmanCode += self.getNYTBuffer(size: 8 - huffmanCode.count)
            writeCode(code: huffmanCode)
        }
        writer.flush();
        return writer.data as Data
    }
    
    private func writeCode(code: String)
    {
        for char in code
        {
            if char == "0"
            {
                self.writer.writeBit(bit: false)
            }
            else
            {
                self.writer.writeBit(bit: true)
            }
        }
    }

    private func getHuffmanCode(valueToBeAdded: String) -> String
    {
        var isNewValue = !tree.stringExists(value: valueToBeAdded)
        var result: String;
        if(isNewValue)
        {
            result = tree.getHuffmanCode(valueToGet: "") + valueToBeAdded
        }
        else
        {
            result = tree.getHuffmanCode(valueToGet: valueToBeAdded)
        }
        tree.addCharToTree(valueToAdd: valueToBeAdded)
        return result
    }
    
    private func bitRepresentation(value: Int) -> String
    {
        var str = String(value, radix: 2)
        str = pad(string: str, toSize: 8)
        return str;
    }
    
    func pad(string : String, toSize: Int) -> String
    {
        var padded = string
        for _ in 0..<(toSize - string.count)
        {
            padded = "0" + padded
        }
        return padded
    }
    
    private func getNYTBuffer(size: Int) -> String
    {
        var result = self.getHuffmanCode(valueToBeAdded: "")
        
        if size < result.count
        {
            result = String(result.prefix(size))
        }
        else if size > result.count
        {
            while result.count < size
            {
                result.append("0")
            }
        }
        return result
    }
}
