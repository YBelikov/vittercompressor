import Foundation

class HuffmanDecoder
{
    private var tree: HuffmanTree
    private var bitWriter: BitWriter
    public init()
    {
        tree = HuffmanTree()
        bitWriter = BitWriter()
    }
    public func decode(dataToDecode: Data) -> String
    {
        var NYTString = ""
        var result = ""
        var loadingNYT = false
        var currentNode: HuffmanTreeNode? = nil;
        let bitReader = BitReader(data: dataToDecode as NSData)
        let size = dataToDecode.count * 8
        for _ in 0..<size
        {
            let bit = bitReader.readBit() ? "1" : "0"
            if !loadingNYT
            {
                if currentNode === nil
                {
                    loadingNYT = true
                }
                else
                {
                    currentNode = self.tree.getNextNode(prev: currentNode!, index: Character(bit))
                    if (bit == "0" && self.tree.rootIsNYT()) || self.tree.isNYT(node: currentNode!)
                    {
                        loadingNYT = true;
                    }
                    else if self.tree.isLeaf(node: currentNode!)
                    {
                        let val = Int((currentNode?.getValue())!, radix: 2)!
                        let ch = Character(UnicodeScalar(val)!)
                        result.append(String(ch))
                        self.tree.addCharToTree(valueToAdd: (currentNode?.getValue())!)
                        currentNode = self.tree.getRoot()
                    }
                    
                }
            }
            else
            {
                NYTString += String(bit)
                if NYTString.count == 8
                {
                    let val = Int(NYTString, radix: 2)!
                    let character = Character(UnicodeScalar(val)!)
                    result.append(String(character))
                    self.tree.addCharToTree(valueToAdd: NYTString)
                    NYTString = ""
                    currentNode = self.tree.getRoot()
                    loadingNYT = false;
                }
            }
        }
        return result
    }
}
