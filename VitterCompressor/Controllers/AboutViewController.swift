import UIKit

class AboutViewController: ViewController {

    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = .clear
        return scrollView
    }()

    private let contentView: UIView = {
        let contentView = UIView()
        contentView.contentMode = .scaleToFill
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.backgroundColor = .clear
        return contentView
    }()
    
    private var userInfo: UserInfo = {
        return Bundle.main.decodeJson("UserInfo.json", of: UserInfo.self)
    } ()
    
    private let userImage: RoundImageView = {
        let imageView = RoundImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let nameLabel: CenteredLable = {
        let label = CenteredLable()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()
    
    private let groupLabel: CenteredLable = {
        let label = CenteredLable()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()
    
    private let recordBookLabel: CenteredLable = {
        let label = CenteredLable()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()

    private let aboutLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        makePreparations()
    }

    private func makePreparations() {
        installTitle()
        setupScrollView()
        setupViews()
    }
 
}

// MARK: - View instalation
extension AboutViewController {
    
    private func setupViews(){
        contentView.addSubview(userImage)
        userImage.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        userImage.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        userImage.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 3/4).isActive = true
        userImage.heightAnchor.constraint(equalTo: userImage.widthAnchor).isActive = true
        userImage.image = self.userInfo.image
        
        contentView.addSubview(nameLabel)
        nameLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        nameLabel.topAnchor.constraint(equalTo: userImage.bottomAnchor, constant: 25).isActive = true
        nameLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 3/4).isActive = true
        nameLabel.text = self.userInfo.name
        
        contentView.addSubview(groupLabel)
        groupLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        groupLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 25).isActive = true
        groupLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 3/4).isActive = true
        groupLabel.text = self.userInfo.group
        
        contentView.addSubview(recordBookLabel)
        recordBookLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        recordBookLabel.topAnchor.constraint(equalTo: groupLabel.bottomAnchor, constant: 25).isActive = true
        recordBookLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 3/4).isActive = true
        recordBookLabel.text = self.userInfo.recordBook
        
        contentView.addSubview(aboutLabel)
        aboutLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        aboutLabel.topAnchor.constraint(equalTo: recordBookLabel.bottomAnchor, constant: 25).isActive = true
        aboutLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 3/4).isActive = true
        aboutLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        aboutLabel.text = self.userInfo.about
    }
    
    private func setupScrollView() {
        view.addSubview(scrollView)
        
        scrollView.addSubview(contentView)
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
    }
    
    
    private func installTitle() {
        self.title = "About"
    }
    
}
