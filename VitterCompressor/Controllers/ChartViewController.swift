import UIKit
import BarChartKit


class ChartViewController: ViewController {
    
    private let diagramView = BarChartView()

    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = .clear
        return scrollView
    }()
    private let frequencyTable: [Character: Int]
    
    init(frequencyTable: [Character: Int]) {
        self.frequencyTable = frequencyTable
        super.init(nibName: nil, bundle: .main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makePreparations()
    }
    
    private func makePreparations() {
        setupScrollView()
        if !frequencyTable.keys.isEmpty {
            installDiagram()
        }
        
    }
    

}

// MARK: - View instalation
extension ChartViewController {
    
    private func installDiagram() {
        
        self.scrollView.addSubview(diagramView)
        diagramView.translatesAutoresizingMaskIntoConstraints = false


        let color = UIColor.blue
        
        var dataElements: [BarChartView.DataSet.DataElement] = []
        let sortedArray = frequencyTable.sorted {
            return $0.value < $1.value
        }
        for (key, value) in sortedArray {
            let de = BarChartView.DataSet.DataElement(date: nil, xLabel: "\(key)", bars: [BarChartView.DataSet.DataElement.Bar(value: Double(value), color: color)])
            
            dataElements.append(de)
        }
        
        let mockBarChartDataSet: BarChartView.DataSet = BarChartView.DataSet(elements: dataElements, selectionColor: color)
        
        diagramView.dataSet = mockBarChartDataSet
        diagramView.barWidth = 10
        diagramView.delegate = self
        
        NSLayoutConstraint.activate(
            [
                diagramView.topAnchor.constraint(equalTo: self.scrollView.safeAreaLayoutGuide.topAnchor, constant: 20),
                diagramView.bottomAnchor.constraint(equalTo: self.scrollView.safeAreaLayoutGuide.bottomAnchor, constant: -20),
                diagramView.widthAnchor.constraint(equalToConstant: CGFloat(diagramView.dataSet?.elements.count ?? 0) * 20),
            ]
        )
        scrollView.contentSize = CGSize(width: CGFloat(diagramView.dataSet?.elements.count ?? 0) * 20, height: 10)
        
    }
    
    private func setupScrollView() {
        view.addSubview(scrollView)
        
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

}


extension ChartViewController: BarChartViewDelegate {
    
    func didSelect(dataElement: BarChartView.DataSet.DataElement, dataSet: BarChartView.DataSet) {
        showAlert(title: "Amount of \"\(dataElement.xLabel)\":", message: "\(Int(dataElement.bars[0].value))")
    }
    
    
}
