import UIKit

class TabBarController: UITabBarController {

    private var navigationControllers: [UINavigationController] {
        return [currentYearNavigationController, historyNavigationController]
    }

    private let currentYearNavigationController: UINavigationController = {

        let controller = UINavigationController(rootViewController: NotesViewController())

        controller.navigationBar.prefersLargeTitles = true

        let defaultImage = UIImage(systemName: "note",
                            withConfiguration: UIImage.SymbolConfiguration(weight: .regular))

        let selectedImage = UIImage(systemName: "note.text",
                            withConfiguration: UIImage.SymbolConfiguration(weight: .regular))

        let tabBarItem = UITabBarItem(title: "Current",
                                      image: defaultImage,
                                      selectedImage: selectedImage)

        controller.tabBarItem = tabBarItem

        return controller
    } ()

    private let historyNavigationController: UINavigationController = {

        let controller = UINavigationController(rootViewController: AboutViewController())

        let defaultImage = UIImage(systemName: "person",
                            withConfiguration: UIImage.SymbolConfiguration(weight: .regular))
        let selectedImage = UIImage(systemName: "person.fill",
                            withConfiguration: UIImage.SymbolConfiguration(weight: .regular))

        let tabBarItem = UITabBarItem(title: "About",
                                      image: defaultImage,
                                      selectedImage: selectedImage)

        controller.tabBarItem = tabBarItem

        return controller
    } ()

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.tintColor = .red

        self.viewControllers = self.navigationControllers
    }

}
