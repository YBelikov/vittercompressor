import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .systemBackground
    }

    func showAlert(title: String = "Ooops!", message: String?) {

        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)

        let alertAction = UIAlertAction(title: "Ok",
                                        style: .default)
        
        alertController.addAction(alertAction)

        self.present(alertController, animated: true)
    }

}

