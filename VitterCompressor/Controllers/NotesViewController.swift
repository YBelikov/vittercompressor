import UIKit
import CoreData

class NotesViewController: ViewController {

    private var notes: [Note] = []

    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)

        tableView.register(UINib(nibName: "NoteTableViewCell", bundle: .main),
                           forCellReuseIdentifier: "Cell")

        tableView.translatesAutoresizingMaskIntoConstraints = false

        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        makePreparations()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.largeTitleDisplayMode = .always
        fetchUpDataBase()
    }

    private func makePreparations() {
        installTableView()
        installTitle()
        installCreateButton()
    }

    @objc
    private func newNoteButtonTapped() {
        let vc = DetailedNoteViewController()

        self.navigationController?.pushViewController(vc, animated: true)
    }

    private func fetchUpDataBase() {
        CoreDataStack.shared.viewContext.performAndWait {
            let request: NSFetchRequest<Note> = Note.fetchRequest()
            request.sortDescriptors = [NSSortDescriptor(key: "noteEditDate", ascending: false)]
            do {
                let allNotes = try request.execute()
            
                self.notes = allNotes
                self.tableView.reloadData()
            } catch {
                showAlert(message: error.localizedDescription)
            }
        }
    }

}

extension NotesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let removedNote = notes.remove(at: indexPath.row)
            CoreDataStack.shared.viewContext.delete(removedNote)
            CoreDataStack.shared.save()
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
}

extension NotesViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        let cell = cell as? NoteTableViewCell
        cell?.install(notes[indexPath.row])

    }

    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)

        let selectedNote = notes[indexPath.row]
        let vc = DetailedNoteViewController(note: selectedNote)
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

// MARK: - View instalation

extension NotesViewController {

    private func installTableView() {
        self.view.addSubview(tableView)

        NSLayoutConstraint.activate(
            [
                tableView.topAnchor.constraint(equalTo: self.view.topAnchor),
                tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
                tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
                tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
            ]
        )

        tableView.delegate = self
        tableView.dataSource = self
    }

    private func installTitle() {
        self.title = "Notes"
    }

    private func installCreateButton() {
        let buttonImage = UIImage(systemName: "square.and.pencil",
                                  withConfiguration: UIImage.SymbolConfiguration(weight: .regular))

        let barItem = UIBarButtonItem(image: buttonImage,
                                      style: .done,
                                      target: self,
                                      action: #selector(newNoteButtonTapped))

        navigationItem.rightBarButtonItem = barItem
    }

}
