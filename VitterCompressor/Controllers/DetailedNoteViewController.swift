import UIKit
import CoreData

class DetailedNoteViewController: ViewController {
    
    private let noteNameField: UITextField = {
        let textField = UITextField(frame: CGRect(x: 0,
                                                  y: 0,
                                                  width: 200,
                                                  height: 22))
        
        textField.text = "New note"
        textField.textAlignment = .center
        
        return textField
    }()
    
    private let textView: UITextView = {
        let textView = UITextView()
        
        return textView
    }()
    
    private var note: Note?
    
    init() {
        super.init(nibName: nil, bundle: nil)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    init(note: Note) {
        self.note = note
        self.noteNameField.text = note.noteName
        self.textView.text = HuffmanDecoder().decode(dataToDecode: note.noteData)
        super.init(nibName: nil, bundle: nil)
    }
    
    private func commonInit() {
        let context = CoreDataStack.shared.container.newBackgroundContext()
        let readContext = CoreDataStack.shared.viewContext
        
        context.performAndWait {
            
            guard let note = NSEntityDescription.insertNewObject(forEntityName: "Note", into: context) as? Note else {
                showAlert(message: "Unable to create note!")
                return
            }
            
            note.noteName = "New note"
            note.noteEditDate = Date()
            note.noteData = Data()
            
            
            do {
                try context.save()
                CoreDataStack.shared.save()
            } catch {
                showAlert(message: error.localizedDescription)
            }
            readContext.performAndWait {
                do {
                    guard let result = try readContext.existingObject(with: note.objectID) as? Note else {
                        showAlert(message: "Unable to open just created note!")
                        return
                    }
                    self.note = result
                } catch {
                    showAlert(message: error.localizedDescription)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makePreparations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.largeTitleDisplayMode = .never
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        CoreDataStack.shared.save()
        self.noteNameField.resignFirstResponder()
        self.textView.resignFirstResponder()
    }
    
    private func makePreparations() {
        installTableView()
        installTitle()
        installKeyboardListener()
        installInfoButton()
    }
    
    @objc
    private func noteNameDidChanged(_ sender: UITextField) {
        guard let noteName = sender.text else {
            return
        }
        self.note?.noteName = noteName
        self.note?.noteEditDate = Date()
    }

    @objc
    private func infoButtonTapped() {
        let frequncyTable = FrequencyTable()
        frequncyTable.calculate(text: self.textView.text)
        let vc = ChartViewController(frequencyTable: frequncyTable.frequencies())

        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension DetailedNoteViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        guard let noteContent = textView.text else {
            return
        }
        self.note?.noteData = HuffmanEncoder().encode(text: noteContent)
        self.note?.noteEditDate = Date()
        self.note?.actualSize = noteContent.count
    }
}

// MARK: - Keyboard notification(hide/appear) listener
extension DetailedNoteViewController {
    
    private func installKeyboardListener() {
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(adjustForKeyboard),
                                       name: UIResponder.keyboardWillHideNotification, object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(adjustForKeyboard),
                                       name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc
    private func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            textView.contentInset = .zero
        } else {
            textView.contentInset = UIEdgeInsets(top: 0,
                                                 left: 0,
                                                 bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom,
                                                 right: 0)
        }
        
        textView.scrollIndicatorInsets = textView.contentInset
        
        let selectedRange = textView.selectedRange
        textView.scrollRangeToVisible(selectedRange)
    }
    
}

// MARK: - View instalation
extension DetailedNoteViewController {
    
    private func installTableView() {
        self.view.addSubview(textView)
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate(
            [
                textView.topAnchor.constraint(equalTo: self.view.topAnchor),
                textView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
                textView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
                textView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
            ]
        )
        
        textView.delegate = self
    }
    
    private func installTitle() {
        self.navigationItem.titleView = noteNameField
        noteNameField.addTarget(self,
                                action: #selector(noteNameDidChanged),
                                for: .editingChanged)
    }

    private func installInfoButton() {
        let buttonImage = UIImage(systemName: "info.circle.fill",
                                  withConfiguration: UIImage.SymbolConfiguration(weight: .regular))

        let barItem = UIBarButtonItem(image: buttonImage,
                                      style: .done,
                                      target: self,
                                      action: #selector(infoButtonTapped))

        navigationItem.rightBarButtonItem = barItem
    }
    

}

